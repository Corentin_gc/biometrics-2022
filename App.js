/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow strict-local
 */

import React from 'react';
import type {Node} from 'react';
import {
  SafeAreaView,
  ScrollView,
  StyleSheet,
  Text,
  View,
  Button,
  TextInput,
} from 'react-native';

import ReactNativeBiometrics from 'react-native-biometrics';
import AsyncStorage from '@react-native-async-storage/async-storage';

const App: () => Node = () => {
  const [firstNameText, onChangeFirstName] = React.useState("");
  const [lastNameText, onChangeLastName] = React.useState("");
  const [isRegistered, setRegistered] = React.useState(false);
  const [isLogin, setLogin] = React.useState(false);
  const [firstName, setFirstName] = React.useState(false);
  const [lastName, setLastName] = React.useState(false);

  const getLocalStorage = async () => {
    const getFirstName = await AsyncStorage.getItem('firstName');
    const getLastName = await AsyncStorage.getItem('lastName');

    if (getFirstName && getLastName){
      setFirstName(getFirstName)
      setLastName(getLastName)
      setRegistered(true)
    }
  }

  getLocalStorage();

  const handleLogin = () => {
    ReactNativeBiometrics.simplePrompt({promptMessage: 'Confirm fingerprint'})
      .then(async (resultObject) => {
        const { success } = resultObject

        if (success) {
          setLogin(true);
          alert('You are successful log')
        }
      })
      .catch(() => {
        alert('biometrics failed')
      })
  }

  const handleRegister = () => {
    if (firstNameText === "" || lastNameText === "") {
      alert('Fields are required')
    }else {
      ReactNativeBiometrics.simplePrompt({promptMessage: 'Confirm fingerprint to register'})
        .then((resultObject) => {
          const { success } = resultObject

          if (success) {
            AsyncStorage.setItem(
              'firstName',
              firstNameText
            );
            AsyncStorage.setItem(
              'lastName',
              lastNameText
            );

            getLocalStorage();
            setRegistered(true);
            alert('You are succesfully registered !')
            setLogin(true);
          }

        })
        .catch(() => {
          alert('biometrics failed')
        })
    }
  }

  return (
    <SafeAreaView>
      <ScrollView
        contentInsetAdjustmentBehavior="automatic">
        <View
          style={styles.view}>
          {isRegistered && (
            <View>
              <Text style={styles.title}>
                Login
              </Text>
              {!isLogin && (
                <View>
                  <Button
                    onPress={handleLogin}
                    title="Click here to login!"
                  />
                </View>
              )}
              {isLogin && (
                <View>
                  <TextInput
                    style={styles.input}
                    value={firstName}
                    editable={false}
                    selectTextOnFocus={false}
                  />
                  <TextInput
                    style={styles.inputLastName}
                    value={lastName}
                    editable={false}
                    selectTextOnFocus={false}
                  />
                </View>
              )}
            </View>
          )}

          {!isRegistered && (
            <View>
              <Text style={styles.title}>
                Register
              </Text>
              <View>
                <TextInput
                  style={styles.input}
                  onChangeText={firstNameText => onChangeFirstName(firstNameText)}
                  placeholder={"First Name"}
                />
                <TextInput
                  style={styles.inputLastName}
                  onChangeText={lastNameText => onChangeLastName(lastNameText)}
                  placeholder={"Last Name"}
                />
                <Button
                  style={{margin:10}}
                  onPress={handleRegister}
                  title="Register"
                />
              </View>
            </View>
          )}
        </View>
      </ScrollView>
    </SafeAreaView>
  );
};

const styles = StyleSheet.create({
  view: {
    margin: 10,
  },
  inputLastName: {
    marginBottom: 10,
    height: 40,
    borderWidth: 1,
    padding: 10,
    marginTop: 10,
  },
  title: {
    fontSize: 24,
    fontWeight: '700',
    marginTop: 10,
    marginBottom: 10,
  },
  highlight: {
    fontWeight: '700',
  },
  input: {
    height: 40,
    borderWidth: 1,
    padding: 10,
    marginTop: 10,
  },
});

export default App;
